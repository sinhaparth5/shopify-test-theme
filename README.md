# Shopify Test Theme



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd shopify-test-theme
git remote add origin https://gitlab.com/sinhaparth5/shopify-test-theme.git
git branch -M main
git push -uf origin main
```

## Shopify CLI
```shell
# shopify watch changes
theme watch
theme watch --allow-live

# shopify push changes
shopify theme push
```

## Tailwind CSS CLI
```shell
# Installing tailwind css 
npm -i -D tailwindcss
npx tailwindcss init

# Watch changes
npx tailwindcss -i ./src/input.css -o ./src/output.css --watch
```