function isMobileDevice() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

const menusElement = document.getElementsByClassName('.menus');

if (isMobileDevice()) {
    menusElement.classList.add('hidden'); // Add a class for hiding (e.g., 'hidden')
} else {
    menusElement.classList.remove('hidden'); // Remove the hiding class on larger screens
}

window.addEventListener('resize', () => {
    if (isMobileDevice()) {
        menusElement.classList.add('hidden');
    } else {
        menusElement.classList.remove('hidden');
    }
});
